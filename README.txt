# README #

This is a discription of how to operate our system. 

Prepare the training set:
1. Go to https://petscan.wmflabs.org/, enter the name of the Wikipedia category in the "Categories" section
2. At "Wikidata" tab, make sure you enter the gender identifier at "Uses items/props" section, for female Q6581072 and for male Q6581097
3. In the "Output" tab, select csv as the output format, and click on "Do it!"
4. Put the exported csv file to the folder "male" and "female" under the folder "train" according to the gender identifier
5. If you only want to classify multiple pages, you can export the csv file by adding your own constrains on petScan and save it to any sub-folders of folder "test"
6. If you want to also evaluate the performance of the classification, be sure your testing csv file is in the right sub-folders according to the gender that your testing points belong to.



Run the classification
*Prepare the training set before any classifications !!!!!
*Please be sure the folder "test" and "train" are in the same directory as the Executable jar file !!!!! 
1. Double click "GenderPrediction.jar" to run the program
2. Select the classification mode that you want to implement
3. If you want to classify the a single page please select "I have the URL of a single page", and the instruction are from 3.1 to 3.3. 
   If you want to classify the csv(multiple pages), please select "I have the csv file" and read from 3.4 to 3.8
   
   
   
3.1 Click on "Initialize the training set", the system will initialize all the training data that you put under the folder "train". The progress bar shows the progress of this session.
	The "traindata_updated.csv" will be created at the same folder when the session is completed. 
3.2 Copy the URL to the textfield, select the value "K", and press "Classify gender". The program should return you the result from KNN algorithm.
3.3 Press "Evaluate" to examine if the classification is identical to the result extracting from Wikidata(Wikidata do have the gender information for the most Wikipedia entities).




3.4 Click on "Initialize testing set", the system will prepare the testing data that you put in the folder "test", it eventually creates the "test_updated" contains the feature values 
	of all testing points. 
3.5 Click on "Initialize training set", the system will initialize all the training data that you put under the folder "train". The progress bar shows the progress of the session.
	The "traindata_updated.csv" will be created at the same folder when the session is completed.
3.6 Select the classification appraoch, there are 6 options, you can either run KNN by specifying the value of K or run Heurstic by selecting the configuration("He vs She","His vs Her" and etc.) 
3.7 The systems returns the "result.csv" to the "test" folder, in the "result.csv" the binary value before the url indicates the gender of the entity(1 for male and 0 for feamle)
3.8 If you want to evaluate the accuracy of the classification, please be sure that the testing points are located in the right gender folder under the folder "test".
    The system will return you the summary of the performance of the classification on the textfield as long as the classification is done. 

4. If you want to import new testing data, please delete "test.csv" , "test_updated.csv", and "result.csv" first

For any questions or comments, feel free to contact the creators of this system at Jonathan_Maas@hotmail.com, or dingmingcheng@163.com 