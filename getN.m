function [] = getN(filename,N)
    a = readtable(filename,'Format','%s%s%s%s%s%s');
    index = randi([1,height(a)],N,1);
    b = table2cell(a);
    for i = 1:length(index)
        c(i,:) = b(index(i),:);
    end
    d = cell2table(c);
    writetable(d,filename,'WriteVariableNames',false)
end

