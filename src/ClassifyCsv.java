import java.awt.Font;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Vector;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

//import classifier.Feature;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JTextArea;
import javax.swing.SwingWorker;
import javax.swing.JProgressBar;
import javax.swing.JRadioButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class ClassifyCsv extends JFrame {

	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JButton btnInitTrainingSet = new JButton("Initialize training set");
    private JButton btnInitTestingSet = new JButton("Initialize testing set");
	private JProgressBar progressBar = new JProgressBar();
    private JProgressBar progressBar_testset = new JProgressBar();
	private JTextArea textArea = new JTextArea();
	private String curdir = System.getProperty("user.dir");
	private String csvFile = curdir+"\\train\\traindata_updated.csv";
	private String testFile = curdir+"\\test\\test.csv";
	private String testFileUpdated = curdir+"\\test\\test_updated.csv";
	private String resultFile = curdir+"\\test\\result.csv";
	private FileReader ts = null;
	private FileReader fs = null;
	private ArrayList<Pronoun> Pronouns= new ArrayList<Pronoun>();
	private JButton btnHome = new JButton("Home");
	private int K;
	private final JRadioButton K1 = new JRadioButton("K=1");
	private final JRadioButton K3 = new JRadioButton("K=3");
	private final JRadioButton K5 = new JRadioButton("K=5");
    private JRadioButton rdbtnHe = new JRadioButton("He vs She");
    private JRadioButton rdbtnHis = new JRadioButton("His vs Her");
    private JRadioButton rdbtnHeHis = new JRadioButton("He+His vs She+Her");
    private String heurstic = null;
	private ButtonGroup buttonGroup = new ButtonGroup();
    private JButton runClassification = new JButton("Run Classification");
    private int trueCountMale = 0;
    private int trueCountFemale = 0;



	public ClassifyCsv() {
		super("Gender predication");
		Pronouns = URLReader.getPronounList();
		initLayout();
		runTask();
	}
	
	
	public void runTask() {
		try {
			fs = new FileReader(csvFile);
		} catch (FileNotFoundException e) {
			
		}
		try {
			ts = new FileReader(testFile);
		} catch (FileNotFoundException e) {
		}
		
			
		try {
			Vector<Feature> existingTestSet = Tools.extractFeaturesFromCSVforCompleteFeature("\\test\\test_updated.csv");
			for(Feature feature : existingTestSet){
				if (feature.getClassLabel() == 1){
					trueCountMale++;
				}else{
					trueCountFemale++;
				}
			}
		} catch (FileNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		
		
		
		
		
		
		btnInitTrainingSet.setToolTipText("Get the feature values for the training set. You can skip this if \"traindata_updated.csv\" is already in the \"train\" folder");
		
		btnInitTrainingSet.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				UpdateTrainData updateTrainData = new UpdateTrainData();
				updateTrainData.execute();
			}
		});
		btnHome.setToolTipText("Back to the home screen");
		
		btnHome.addMouseListener(new MouseAdapter() {			
			@Override
			public void mouseClicked(MouseEvent e) {
				MainActivity frame = new MainActivity();
				frame.setVisible(true);
				ClassifyCsv.this.dispose();
			}
		});
		btnInitTestingSet.setToolTipText("Get the feature values for the testing set. You can skip this if \"test_updated.csv\" is already in the \"test\" folder");
		
		btnInitTestingSet.addMouseListener(new MouseAdapter() {
        	@Override
        	public void mouseClicked(MouseEvent e) {
        		UpdateTestData updateTestData = new UpdateTestData();
        		updateTestData.execute();
			}
		});

		
		runClassification.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent e) {
        		
        		if (K1.isSelected() || K5.isSelected()|| K3.isSelected() ) {
					Predict predict = new Predict();
					predict.execute();
				}else if (rdbtnHe.isSelected()||rdbtnHeHis.isSelected()||rdbtnHis.isSelected()) {
					RunHeurstic runHeurstic = new RunHeurstic();
					runHeurstic.execute();
				}
        	}
        });
		
	}
	
	
	class RunHeurstic extends SwingWorker<Void, Void>{

		@Override
		protected Void doInBackground() throws Exception {
			// TODO Auto-generated method stub
			if (rdbtnHe.isSelected()) {
				heurstic = "He vs She";
			}else if (rdbtnHeHis.isSelected()) {
				heurstic = "He+His vs She+Her";
			}else if (rdbtnHis.isSelected()) {
				heurstic = "His vs Her";
			}
			try {
				ts = new FileReader(testFile);
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
			}
			if (ts==null) {
				textArea.setText("Initialize the testing set first");
			}else {
				Vector<Feature> result = CSVReader.applyHeursticForCsv(heurstic);
				String recordAsCsv = result.stream()
				        .map(Feature::toCsvRow)
				        .collect(Collectors.joining(System.getProperty("line.separator")));
				try {
					PrintWriter pw = new PrintWriter(new File(resultFile));
			        StringBuilder sb = new StringBuilder();
			        sb.append(recordAsCsv);
			        pw.write(sb.toString());
			        pw.close();
				} catch (FileNotFoundException e1) {
					e1.printStackTrace();
				}
				double maleCount = 0;
				double femaleCount = 0;
				for(Feature feature:result){
					if (feature.getClassLabel()==1) {
						maleCount++;
					}else{
						femaleCount++;
					}
				}
				double totalNumber = maleCount+femaleCount;
				double errorRate = 100*(Math.abs((maleCount+femaleCount)/2-maleCount))/totalNumber;
				double correctRate = URLReader.round(100-errorRate,2);
				textArea.setText("  Heurstic approach:"+heurstic+"\n  Number of testing points:"+Math.round(totalNumber)+"\n  Classified as Male: "+Math.round(maleCount)+" True: "+trueCountMale+"\n  Classified as Female: "+Math.round(femaleCount)+" True: "+trueCountFemale+"\n  Accuracy:"+correctRate+"%");
			}
            return null;
        }  
			
	}
	
	
	class UpdateTestData extends SwingWorker<Void, Void>{

		@Override
		protected Void doInBackground() {
			btnInitTestingSet.setEnabled(false);
			int progress = 0;
			setProgress(0);
			Vector<Feature> newTestSet = CSVReader.updateTestingSet();
			for(int i=0;i<newTestSet.size();i++ ){
				String wikiPediaURL = newTestSet.get(i).getUrl();
				wikiPediaURL = wikiPediaURL.replace(",", "");
				String html = URLReader.getHTML(wikiPediaURL);
				URLReader.countPronounForFeature(html.toLowerCase(), newTestSet.get(i), Pronouns);
				if (newTestSet.get(i).getClassLabel() == 1){
					trueCountMale++;
				}else{
					trueCountFemale++;
				}
				
				progress = (i*100)/newTestSet.size()+1;
				progressBar_testset.setValue(progress);
			}
			String recordAsCsv = newTestSet.stream()
			        .map(Feature::toCsvRow)
			        .collect(Collectors.joining(System.getProperty("line.separator")));
			
			try {
				PrintWriter pw = new PrintWriter(new File(testFileUpdated));
		        StringBuilder sb = new StringBuilder();
		        sb.append(recordAsCsv);
		        pw.write(sb.toString());
		        pw.close();
			} catch (FileNotFoundException e1) {
				e1.printStackTrace();
			}
			btnInitTestingSet.setEnabled(true);
			try {
				fs = new FileReader(csvFile);
			} catch (FileNotFoundException e) {
				
			}
			try {
				ts = new FileReader(testFile);
			} catch (FileNotFoundException e) {
			}
			return null;
		}
	}
	
	
	
	class UpdateTrainData extends SwingWorker<Void, Void> {
        @Override
        public Void doInBackground() {
        	btnInitTrainingSet.setEnabled(false);
			int progress = 0;
			setProgress(0);
			Vector<Feature> newTrainSet = CSVReader.updateTrainingSet();
			for(int i=0;i<newTrainSet.size();i++ ){
				String wikiPediaURL = newTrainSet.get(i).getUrl();
				wikiPediaURL = wikiPediaURL.replace(",", "");
				String html = URLReader.getHTML(wikiPediaURL);
				URLReader.countPronounForFeature(html.toLowerCase(), newTrainSet.get(i), Pronouns);
				progress = (i*100)/newTrainSet.size()+1;
				progressBar.setValue(progress);
			}
			
			String recordAsCsv = newTrainSet.stream()
			        .map(Feature::toCsvRow)
			        .collect(Collectors.joining(System.getProperty("line.separator")));
			
			try {
				PrintWriter pw = new PrintWriter(new File(csvFile));
		        StringBuilder sb = new StringBuilder();
		        sb.append(recordAsCsv);
		        pw.write(sb.toString());
		        pw.close();
			} catch (FileNotFoundException e1) {
				e1.printStackTrace();
			}
			btnInitTrainingSet.setEnabled(true);
			try {
				fs = new FileReader(csvFile);
			} catch (FileNotFoundException e) {
				
			}
			try {
				ts = new FileReader(testFile);
			} catch (FileNotFoundException e) {
			}
            return null;
        }  
    }

	
	class Predict extends SwingWorker<Void, Void> {
        @Override
        public Void doInBackground() {
        	
        	if (K1.isSelected()) {
				K = 1;
			}else if (K3.isSelected()) {
				K = 3;
			}else if (K5.isSelected()) {
				K = 5;
			}
        	      	
        	
			try {
				ts = new FileReader(testFile);
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
			}
			if (ts==null) {
				textArea.setText("Initialize the testing set first");
			}
			if (fs==null) {
				textArea.setText("Initialize the training set first");
			}
			
			else {
				Vector<Feature> result = CSVReader.applyKnnforCSV(K);
				String recordAsCsv = result.stream()
				        .map(Feature::toCsvRow)
				        .collect(Collectors.joining(System.getProperty("line.separator")));
				try {
					PrintWriter pw = new PrintWriter(new File(resultFile));
			        StringBuilder sb = new StringBuilder();
			        sb.append(recordAsCsv);
			        pw.write(sb.toString());
			        pw.close();
				} catch (FileNotFoundException e1) {
					e1.printStackTrace();
				}
				double maleCount = 0;
				double femaleCount = 0;
				for(Feature feature:result){
					if (feature.getClassLabel()==1) {
						maleCount++;
					}else{
						femaleCount++;
					}
				}
				double totalNumber = maleCount+femaleCount;
				//double errorRate = 100*(Math.abs((maleCount+femaleCount)/2-maleCount))/totalNumber;
				double errorRate = 100*(Math.abs((maleCount-trueCountMale)))/totalNumber;
				double correctRate = URLReader.round(100-errorRate,2);
				textArea.setText("  KNN"+K+"\n  Number of testing points:"+Math.round(totalNumber)+"\n  Classified as Male: "+Math.round(maleCount)+" True: "+trueCountMale+"\n  Classified as Female: "+Math.round(femaleCount)+" True: "+trueCountFemale+"\n  Accuracy:"+correctRate+"%");
			}
            return null;
        }  
    }
	
	
	
	
	
	public void initLayout() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 755, 442);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		btnInitTrainingSet.setFont(new Font("Arial", Font.PLAIN, 20));
		btnInitTrainingSet.setBounds(14, 91, 215, 33);
		contentPane.add(btnInitTrainingSet);
		
		textArea.setBounds(14, 234, 707, 143);
		textArea.setFont(new Font("Arial", Font.PLAIN, 20));
		textArea.setEditable(false);
		contentPane.add(textArea);
		
		progressBar.setBounds(243, 92, 427, 32);
		progressBar.setMinimum(0);
		progressBar.setStringPainted(true);
		progressBar.setFont(new Font("Arial", Font.PLAIN, 20));
		contentPane.add(progressBar);
		
		btnHome.setBounds(608, 0, 113, 27);
		btnHome.setFont(new Font("Arial", Font.PLAIN, 20));
		contentPane.add(btnHome);
				
		K1.setBounds(240, 139, 73, 27);
		K1.setFont(new Font("Arial", Font.PLAIN, 20));
		contentPane.add(K1);
		
		K3.setBounds(380, 139, 73, 27);
		K3.setFont(new Font("Arial", Font.PLAIN, 20));
		K3.setSelected(true);
		contentPane.add(K3);
		
		K5.setBounds(520, 140, 73, 27);
		K5.setFont(new Font("Arial", Font.PLAIN, 20));
		contentPane.add(K5);
		
        
        btnInitTestingSet.setBounds(14, 45, 215, 33);
        btnInitTestingSet.setFont(new Font("Arial", Font.PLAIN, 20));
        contentPane.add(btnInitTestingSet);
        
        progressBar_testset.setBounds(243, 47, 427, 33);
        progressBar_testset.setMinimum(0);
        progressBar_testset.setStringPainted(true);
        progressBar_testset.setFont(new Font("Arial", Font.PLAIN, 20));
        contentPane.add(progressBar_testset);
        
        rdbtnHe.setBounds(240, 186, 117, 27);
        rdbtnHe.setFont(new Font("Arial", Font.PLAIN, 20));
        contentPane.add(rdbtnHe);
        
        rdbtnHis.setBounds(380, 186, 129, 30);
        rdbtnHis.setFont(new Font("Arial", Font.PLAIN, 20));
        contentPane.add(rdbtnHis);
        
        rdbtnHeHis.setBounds(520, 186, 224, 30);
        rdbtnHeHis.setFont(new Font("Arial", Font.PLAIN, 20));
        rdbtnHeHis.setSelected(true);
        contentPane.add(rdbtnHeHis);
        
        buttonGroup.add(rdbtnHe);
        buttonGroup.add(rdbtnHis);
        buttonGroup.add(rdbtnHeHis);
        buttonGroup.add(K1);
        buttonGroup.add(K3);
        buttonGroup.add(K5);
       
        runClassification.setBounds(14, 142, 215, 71);
        runClassification.setFont(new Font("Arial", Font.PLAIN, 20));
        contentPane.add(runClassification);
        
        
        
	}
}


