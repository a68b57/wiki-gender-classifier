
public class Pronoun {
	private float count;
	private String pronoun;
	public Pronoun(float count, String pronoun) {
		super();
		this.count = count;
		this.pronoun = pronoun;
	}
	public float getCount() {
		return count;
	}
	public void setCount(float count) {
		this.count = count;
	}
	public String getPronoun() {
		return pronoun;
	}
	public void setPronoun(String pronoun) {
		this.pronoun = pronoun;
	}
	
}
