

//import classifier.DataPoint;
//import classifier.Classifier;
//import classifier.Feature;
import java.util.*;

public class KNN extends Classifier{
	private Vector<Feature> dataset;
	private int K;
	private int N;
	private int numClasses;
	
	public KNN(int k, int numclasses) {
		// TODO Auto-generated constructor stub
		this.K = k;
		this.numClasses = numclasses;
	}

	@Override
	public void initialize() {
		this.initialize(1, 2);
	}
	
	/*
	@Override
	public void initialize(int k, int numclasses){
		this.K = k;
		this.numClasses = numclasses;
	}
	*/
	
	@Override
	public void train( Vector<Feature> A) {
		this.dataset = A;
		this.N = A.size();
		// TODO Auto-generated method stub	
	}
	
	

	//@Override
	public int classify(Feature A) {
		// TODO Auto-generated method stub
		//Vector<Double> distances = new Vector(this.N);
		
		
		int[] neighbours = computeClosestPoints(A);
		
		//System.out.print("classifying feature: " + A.toString() +"\nClosest features:\n");
		
		int[] closestClasses = new int[this.numClasses];

		for (int i = 0; i<this.K;++i){ //for every of the neighbours...
			
			closestClasses[ this.dataset.elementAt(neighbours[i]).getClassLabel()] += 1; //increment the counts
			//System.out.print(this.dataset.elementAt(neighbours[i]).toString() + "\n");
		}
		
		int max = 0;
		int idx = 0;
		for (int i = 0; i<this.numClasses; ++i){
			if (closestClasses[i] > max){
				max = closestClasses[i];
				idx = i;
			}
		}
		return idx;
	}
	
	
	private int[] computeClosestPoints(DataPoint A){
		
		double[] distances = new double[this.N];//{9, 2, 8, 4, 8, 3, 7, 9, 12, 7}; //index 1 and 5 are closest
		int[] closestIndexes = new int[this.K];
		//int distLength = distances.length;
		//int numNeighbours = this.K;
		
		
		
		for (int i=0;i<this.N;++i){ //for all distances
			double distance = this.dataset.elementAt(i).computeDistance(A); //distances[i]; //get value
			distances[i] = distance;
			for (int j=0;j<this.K;++j){ //for every till-now closest points
				double curDist = distances[closestIndexes[j]]; //get distance of specific current closest point
				if (curDist > distance){ //if current distance smaller than recorded
					closestIndexes = insertAndShiftValues(closestIndexes, j, i);
					break;
				}
			}
		}
		return closestIndexes;
	}

	
	
	private static int[] insertAndShiftValues(int[] values, int index, int val)
	{
		if(index == values.length-1){
			values[index] = val;
			return values;
		}else{
			int nextval = values[index];
			values[index] = val;
			return insertAndShiftValues(values, (index+1), nextval);
		}
	}

}
