

//import classifier.DataPoint;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Feature extends DataPoint{
	
	
	private String URL;
	
	public Feature(){
		
	}
	
	public Feature(double[] featurevalues, int classlbl, String url){
		super(featurevalues, classlbl);
		this.URL = url;
	}
	
	
	public String getUrl(){
		return this.URL.replace(",", "");
	}
	
	public void setUrl(String newUrl){
		this.URL = newUrl;
	}
	
	public DataPoint toDataPoint(){
		return new DataPoint(this.getFeatureValues(), this.getClassLabel());
	}
	
	public String toString(){
		return super.toString() + ", url: " + this.getUrl(); 
	}
	
	
	public String toCsvRow() {
	    return Stream.of(String.valueOf(this.getFeatureValues()[0]), String.valueOf(this.getFeatureValues()[1]), String.valueOf(this.getFeatureValues()[2]), String.valueOf(this.getFeatureValues()[3]), String.valueOf(this.getFeatureValues()[4]), String.valueOf(this.getFeatureValues()[5]), String.valueOf(this.getFeatureValues()[6]), String.valueOf(this.getFeatureValues()[7]),String.valueOf(this.getClassLabel()),this.getUrl())
	            .map(value -> value.replaceAll("\"", "\"\""))
	            .map(value -> Stream.of("\"", ",").anyMatch(value::contains) ? "\"" + value + "\"" : value)
	            .collect(Collectors.joining(","));
	}
}
