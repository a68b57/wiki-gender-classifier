import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Scanner;
import java.util.Vector;

//import classifier.Feature;

public class Tools {
	
	public static Vector<Feature> extractFeaturesFromCSV(String csvFilename) throws FileNotFoundException {
		Vector<Feature> vectDataset = new Vector<Feature>();
		String curdir = System.getProperty("user.dir");
		FileReader fs = new FileReader(curdir + csvFilename);
		Scanner scanner = new Scanner(fs);
		double[] featarray;
		double feat1;
		double feat2;
		int classlabl;
		String url;
		Feature feat;


		scanner.useDelimiter(",");
		while(scanner.hasNextLine()){
			featarray = new double[2];
			feat1 = scanner.nextDouble();
			feat2 = scanner.nextDouble();
			classlabl = scanner.nextInt();
			
			url = scanner.nextLine(); //findInLine("\n");//scanner.next();
			featarray[0] = feat1;
			featarray[1] = feat2;
			//scanner.skip("\n");
			feat = new Feature(featarray, classlabl, url);
			vectDataset.addElement(feat);
			//printFlag("printing read in data as feature: \n");
			//printFlag(feat.toString()+"\n");
		}

		//Feature[] dataset = new Feature[vectDataset.size()];
		//vectDataset.toArray(dataset);
		scanner.close();
		try {
			fs.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return vectDataset;
	}
	
	
	public static Vector<Feature> extractFeaturesFromCSVforCompleteFeature(String csvFilename) throws FileNotFoundException {
		Vector<Feature> vectDataset = new Vector<Feature>();
		String curdir = System.getProperty("user.dir");
		FileReader fs = new FileReader(curdir + csvFilename);
		Scanner scanner = new Scanner(fs);
		//String readline;
		double[] featarray;
		double feat1;
		double feat2;
		double feat3;
		double feat4;
		double feat5;
		double feat6;
		double feat7;
		double feat8;
		int classlabl;
		String url;
		Feature feat;


		scanner.useDelimiter(",");
		while(scanner.hasNextLine()){
			featarray = new double[8];
			feat1 = scanner.nextDouble();
			feat2 = scanner.nextDouble();
			feat3 = scanner.nextDouble();
			feat4 = scanner.nextDouble();
			feat5 = scanner.nextDouble();
			feat6 = scanner.nextDouble();
			feat7 = scanner.nextDouble();
			feat8 = scanner.nextDouble();
			classlabl = scanner.nextInt();
			
			url = scanner.nextLine(); //findInLine("\n");//scanner.next();
			featarray[0] = feat1;
			featarray[1] = feat2;
			featarray[2] = feat3;
			featarray[3] = feat4;
			featarray[4] = feat5;
			featarray[5] = feat6;
			featarray[6] = feat7;
			featarray[7] = feat8;
			//scanner.skip("\n");
			
			feat = new Feature(featarray, classlabl, url);
			
			vectDataset.addElement(feat);
			
			//printFlag("printing read in data as feature: \n");
			//printFlag(feat.toString()+"\n");
		}

		//Feature[] dataset = new Feature[vectDataset.size()];
		//vectDataset.toArray(dataset);
		scanner.close();
		try {
			fs.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return vectDataset;
	}
	
	
	@SuppressWarnings("null")
	public static void collectTrainingData(String traindirectory) throws FileNotFoundException {
		String curdir = System.getProperty("user.dir");
		File m = null;
		File[] mpaths;
		File f = null;
		File[] fpaths;
		//File test = new File(curdir+ "\\train\\test22.csv");
		
		try{      
			
			/* */
			// start writing stuff to file
			//FileWriter fwrite = new FileWriter(test.toString()); //creating the file (will overwrite if already exists)
			
			/* */
			
			
			// create new file
			f = new File(curdir + traindirectory + "\\female\\");
			m = new File(curdir + traindirectory + "\\male\\");
			// returns pathnames for files and directory
			mpaths = m.listFiles();
			fpaths = f.listFiles();
			
			// for each pathname in pathname array
			if(mpaths != null){
			for(File mpath:mpaths)
			{
				// prints file and directory paths
				System.out.println(mpath);
			}
			}else{
				System.out.println(f.toString() + "is not a directory");
			}
			if (fpaths != null){
			for(File fpath:fpaths) //printing all from female
			{
				// prints file and directory paths
				System.out.println(fpath);
			}
			}else{
				System.out.println(fpaths.toString() + "is not a directory");
			}
			
			
			
		}catch(Exception e){
			// if any error occurs
			e.printStackTrace();
		}
		/*
				scanner.useDelimiter(",");
		while(scanner.hasNextLine()){
			featarray = new double[2];
			feat1 = scanner.nextDouble();
			feat2 = scanner.nextDouble();
			classlabl = scanner.nextInt();
			
			url = scanner.nextLine(); //findInLine("\n");//scanner.next();
			featarray[0] = feat1;
			featarray[1] = feat2;
			//scanner.skip("\n");
			
			feat = new Feature(featarray, classlabl, url);
			
			vectDataset.addElement(feat);
			
			//printFlag("printing read in data as feature: \n");
			//printFlag(feat.toString()+"\n");
		}

		//Feature[] dataset = new Feature[vectDataset.size()];
		//vectDataset.toArray(dataset);
		scanner.close();
		try {
			fs.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		*/
		
	}
	
	public static void ProcessRawCSV(String Rawcsvpath, String newCsvpath, int label) throws IOException{
		String urlbase = "https://en.wikipedia.org/?curid=";
		String lineToWrite = "";
		
		File in = null;
		//Path path = Paths.get(newCsvpath);
		File f = null;
		
		in = new File(Rawcsvpath);
		try {
			
			f = new File(newCsvpath);
			FileWriter writ = null;
			if (f.exists()){ //the file already existed so we must write to it
				//printFlag("we got here");
				writ = new FileWriter(f, true);
			}else{ //the file needs to be created
				f.createNewFile();
				writ = new FileWriter(f);
				
			}
			
			
			FileReader fread = new FileReader(Rawcsvpath);
			Scanner fscan = new Scanner(fread);
			fscan.useDelimiter(",");
			//fscan.skip("\"number\",\"title\",\"pageid\",\"namespace\",\"length\",\"touched\"");
			//fscan.nextLine();
			while(fscan.hasNextLine()){
				//fscan.skip("\"number\",\"title\",\"pageid\",\"namespace\",\"length\",\"touched\"");
				
				fscan.next(); //skip one
				fscan.next(); //skip second
				
				String pageID = fscan.next(); //third one is pageID

				lineToWrite = "0,0," + label + "," + urlbase+pageID.replaceAll("^\"|\"$", ""); // line to write contains zeros for future feature values, 
																							//then followed by the label, followed by the link, of which
																						//the pageID extracted is stripped from all quotation marks
				
				printFlag(lineToWrite);
				printFlag("\n");
//				lineToWrite = label + "," + urlbase + pageID;
				
				
				//WriteToCSV(newCsvpath, lineToWrite);
				writ.write(lineToWrite + "\n");
				
				
				
				fscan.nextLine();
			}
			
			fscan.close();
			fread.close();
			writ.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}
	

	public static void ProcessRawCSV(File Rawcsvpath, String newCsvpath, int label) throws IOException{
		//String urlbase = "https://en.wikipedia.org/?curid=";
		String urlbase = "https://en.wikipedia.org/wiki/Special:Export/";
		String lineToWrite = "";
		
		File in = null;
		Path path = Paths.get(newCsvpath);
		File f = null;
		
		in = Rawcsvpath;
		try {
			
			f = new File(newCsvpath);
			FileWriter writ = null;
			if (f.exists()){ //the file already existed so we must write to it
				//printFlag("we got here");
				writ = new FileWriter(f, true);
			}else{ //the file needs to be created
				f.createNewFile();
				writ = new FileWriter(f);
				
			}
			
			
			FileReader fread = new FileReader(Rawcsvpath);
			Scanner fscan = new Scanner(fread);
			fscan.useDelimiter(",");
			//fscan.nextLine();
			//fscan.skip("\"number\",\"title\",\"pageid\",\"namespace\",\"length\",\"touched\"");
			while(fscan.hasNextLine()){
				//fscan.skip("\"number\",\"title\",\"pageid\",\"namespace\",\"length\",\"touched\"");
				
				fscan.next(); //skip one
				//fscan.next(); //skip second
				
				String pageID = fscan.next(); //third one is pageID

				lineToWrite = "0,0," + label + "," + urlbase+pageID.replaceAll("^\"|\"$", ""); // line to write contains zeros for future feature values, 
																							//then followed by the label, followed by the link, of which
																						//the pageID extracted is stripped from all quotation marks
				
				//printFlag(lineToWrite);
				//printFlag("\n");
//				lineToWrite = label + "," + urlbase + pageID;
				
				
				//WriteToCSV(newCsvpath, lineToWrite);
				writ.write(lineToWrite + "\n");
				
				
				
				fscan.nextLine();
			}
			
			fscan.close();
			fread.close();
			writ.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
	}
	
	public static void WriteToCSV(String csvFile, String line){
		
		Path path = Paths.get(csvFile);
		File f = null;
		
		try{
		f = new File(csvFile);
		
		if (f.exists()){ //the file already existed so we must write to it
			//printFlag("we got here");
			FileWriter writ = new FileWriter(f, true);
			
			writ.append(line + "\n");
			writ.close();
		}else{ //the file needs to be created
			f.createNewFile();
			FileWriter writ = new FileWriter(f);
			writ.write(line + "\n");
			writ.close();
		}
		
		
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	
	public static void printAsFeatures(String csvFilename) throws FileNotFoundException {
		String curdir = System.getProperty("user.dir");
		FileReader fs = new FileReader(curdir + csvFilename);
		Scanner scanner = new Scanner(fs);
		String readline;
		double[] featarray;
		double feat1;
		double feat2;
		int classlabl;
		String url;
		Feature feat;

		scanner.useDelimiter(",");
		while(scanner.hasNextLine()){
			featarray = new double[2];
			feat1 = scanner.nextDouble();
			feat2 = scanner.nextDouble();
			classlabl = scanner.nextInt();
			
			url = scanner.nextLine(); //findInLine("\n");//scanner.next();
			featarray[0] = feat1;
			featarray[1] = feat2;
			//scanner.skip("\n");
			
			feat = new Feature(featarray, classlabl, url);
			
			//printFlag("printing read in data as feature: \n");
			//printFlag(feat.toString()+"\n");
		}
		
		
	}
	
	public static void printFile(String csvFilename) throws FileNotFoundException {
		String curdir = System.getProperty("user.dir");
		FileReader fs = new FileReader(curdir + csvFilename);
		Scanner scanner = new Scanner(fs);
		String readline;
		double dub;
		int in;
		String txt;
		
		
		
		scanner.useDelimiter(",");
		while(scanner.hasNext()){
        	//printFlag("next: \n");
        	if (scanner.hasNextInt()){
	       		//printFlag("we read a int: \n");
	       		in = scanner.nextInt();
	       		//printFlag(in + "\n");
	       		scanner.useDelimiter("\n");
        	} else 
        		if (scanner.hasNextDouble()){
	        		//printFlag("we read a double: \n");
	        		dub = scanner.nextDouble();
	        		//printFlag(dub + "\n");
	        	}else if (scanner.hasNext()){
		        		//printFlag("we something interpretable as a string: \n");
		        		txt = scanner.next();
		        		//printFlag(txt + "\n");
		        		scanner.useDelimiter(",");
	        	}
        	
        }    
		scanner.close();
		/* */

		
		
		
		/*	//this block of code sucesfully copes with reading of entire lines
		scanner.useDelimiter("\n");
        
		while(scanner.hasNext()){
        	printFlag("next line: \n");
        	readline = scanner.next();
        	printFlag(readline + "\n");
        }    /* */
        
	}
	
	public static Vector<Feature> FileToFeatures(String csvFilename) throws FileNotFoundException {
		String curdir = System.getProperty("user.dir");
		
		Vector<Feature> dataset = new Vector();
		
		FileReader fs = new FileReader(curdir + csvFilename);
		
		Scanner scanner = new Scanner(fs);
		
        scanner.useDelimiter("\n");
        
        //printFlag("B");
        String header = scanner.next();
       // header = scanner.next();
        while(scanner.hasNext()){
        	//printFlag("nextline: ");
        	//String entry = scanner.next();
            Feature next = LineToFeature(scanner);
            //System.out.println(next);
            if (next.getFeatureValues() != null){
                dataset.add(next);
            }
            scanner.useDelimiter("\n");
        }
        
        //printFlag("C");
        
        scanner.close();
		
		
		return dataset;
	}
	
	public static Feature LineToFeature(Scanner scanner){
		
		String url;
		Vector<Double> featureValues= new Vector<Double>(0);
		int classlabel = 0;
		
		scanner.useDelimiter(",");
		
		//reading the feature vector
		while (scanner.hasNextDouble()){
			if (scanner.hasNextInt()){
				classlabel = scanner.nextInt();
				break;
			}else{
				featureValues.addElement(scanner.nextDouble());
			}
		}
		//read the class:
//		classlabel = scanner.nextInt();
		//read the URL
		url = scanner.nextLine();
		double[] featurevals = new double[(featureValues.size())];
		for (int i=0;i<featureValues.size();++i){
			featurevals[i] =  featureValues.elementAt(i);
		}
		
		Feature result = new Feature(featurevals, classlabel, url);
/*		result.setFeatureValues(featurevals);
		result.setClassLabel(classlabel);
		result.setUrl(url); */
		
		return result;
	}
	
	public static void printFlag(String A){
		System.out.print(A);
	}

}
