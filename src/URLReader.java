
import java.io.IOException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.jsoup.Jsoup;
import org.jsoup.select.Elements;

//import classifier.Feature;

public class URLReader {
	
	
	static org.jsoup.nodes.Document document = null;
	static String title = null;
	
	 public static String getHTML(String URL) {
		 String content = null;
		 URLConnection connection= null;
		 try {
			  connection =  new URL(URL).openConnection();
			  @SuppressWarnings("resource")
			Scanner scanner = new Scanner(connection.getInputStream());
			  scanner.useDelimiter("\\Z");
			  content = scanner.next();
			}catch ( Exception ex ) {
			    ex.printStackTrace();
			}
		return content;
    }
	 
	 public static String getWikidataLink(String url) {
		 
		try {
			document = Jsoup.connect(url).get();
		} catch (IOException e) {
			e.printStackTrace();
		}
		 Elements li = document.select("li[id='t-wikibase']");
		 String hyperlink = li.first().getElementsByAttribute("href").attr("href");
		 return hyperlink;
	 }
	 
	 
	 public static String getWikidataID(String url) {
		 
		 try {
				document = Jsoup.connect(url).get();
			} catch (IOException e) {
				e.printStackTrace();
			}
		 System.out.println("done");
		 	title = getTitle(url);
		 	title = title.replace(" ", "+");
			new Get().start();
			return null;

		 }
	 
	 
	 
	 static class Get extends Thread{
		HttpClient client = HttpClients.createDefault();
		@Override
		public void run() {
			HttpGet get = new HttpGet("https://en.wikipedia.org/w/api.php?action=query&format=json&prop=pageprops&titles="+title+"&ppprop=wikibase_item");
			try {
				HttpResponse response = client.execute(get);
				HttpEntity entity = response.getEntity();
				String result = EntityUtils.toString(entity,"UTF-8");
				System.out.println(result);
				
				
			} catch (ClientProtocolException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			
			super.run();
		}
	}
	 
	 
	 
	 
	 public static String getTitle(String url) {
			 org.jsoup.nodes.Element titleElement = document.select("h1").first();
			 String title = titleElement.text();
			 return title;
	}
	 
	 public static String evaluation(String url) {
		 String gender = null;
		 String wikiHtml = getHTML(url);
		 if (wikiHtml.contains("Q6581072")) {
			gender = " is female";
		}else if (wikiHtml.contains("Q6581097")) {
			gender = " is male";
		}
		else gender = " is not a person";
		 return gender;
	}
	 
	 
//	 public static Feature countPronoun(String html,ArrayList<Pronoun> pronoun) {
//		 
//		 Feature feature;
//		 for(int j = 0;j<pronoun.size();j++){
//			 
//			 int i = 1;
//			 Pattern p = Pattern.compile(pronoun.get(j).getPronoun());
//			 Matcher m = p.matcher( html.toLowerCase() );
//			 while (m.find()) {
//			     i++;
//			 }
//			 pronoun.get(j).setCount(i);
//		 }
//		 		 
//		 feature = new Feature();
//		 double pronounRatio;
//		 double possessiveRatio;
//		 pronounRatio = pronoun.get(0).getCount()/(pronoun.get(0).getCount()+pronoun.get(1).getCount());
//		 possessiveRatio = pronoun.get(2).getCount()/(pronoun.get(2).getCount()+pronoun.get(3).getCount());
//		 double[] feaVector = {0,0};
//		 feaVector[0] = round(pronounRatio,2);
//		 feaVector[1] = round(possessiveRatio,2);
//		 feature.setFeatureValues(feaVector);
//		 return feature;
//	}
	 
	 
	public static Feature countPronouns(String html,ArrayList<Pronoun> pronoun) {
		 
		 Feature feature = new Feature();
		 for(int j = 0;j<pronoun.size();j++){
			 int i = 1;
			 Pattern p = Pattern.compile(pronoun.get(j).getPronoun());
			 Matcher m = p.matcher( html.toLowerCase() );
			 while (m.find()) {
			     i++;
			 }
			 pronoun.get(j).setCount(i);
		 }
		 		 
		 float countHe = pronoun.get(0).getCount();
		 float countShe = pronoun.get(1).getCount();
		 
		 float countHis = pronoun.get(2).getCount();
		 float countHer = pronoun.get(3).getCount();
		 
		 float countHeHis = countHe+countHis;
		 float countSheHer = countShe+countHer;
		 
		 	 
		 double pronounRatio;
		 double possessiveRatio;
		 if ((countHe+countShe)==0) {
			 pronounRatio = 0.5;
		}else{
		 pronounRatio = pronoun.get(0).getCount()/(pronoun.get(0).getCount()+pronoun.get(1).getCount());}
		 if (countHer+countHis == 0) {
			 possessiveRatio = 0.5;
		}else{
		 possessiveRatio = pronoun.get(2).getCount()/(pronoun.get(2).getCount()+pronoun.get(3).getCount());}

		 double[] feaVector = {0,0,0,0,0,0,0,0};
			 feaVector[0] = Math.round(countHe);
			 feaVector[1] = Math.round(countShe);
			 feaVector[2] = Math.round(countHis);
			 feaVector[3] = Math.round(countHer);
			 feaVector[4] = Math.round(countHeHis);
			 feaVector[5] = Math.round(countSheHer);
			 feaVector[6] = round(pronounRatio,2);
			 feaVector[7] = round(possessiveRatio,2);
		 feature.setFeatureValues(feaVector);
		 return feature;
	}
	 
	 
	
	 
	 public static Feature countPronounForFeature(String html,Feature feature,ArrayList<Pronoun> pronoun) {
		 for(int j = 0;j<pronoun.size();j++){
			 int i = 1;
			 Pattern p = Pattern.compile(pronoun.get(j).getPronoun());
			 Matcher m = p.matcher( html.toLowerCase() );
			 while (m.find()) {
			     i++;
			 }
			 pronoun.get(j).setCount(i);
		 }
		 		 
		 
		 
		 float countHe = pronoun.get(0).getCount();
		 float countShe = pronoun.get(1).getCount();
		 
		 float countHis = pronoun.get(2).getCount();
		 float countHer = pronoun.get(3).getCount();
		 
		 float countHeHis = countHe+countHis;
		 float countSheHer = countShe+countHer;
		 
		 	 
		 double pronounRatio;
		 double possessiveRatio;
		 pronounRatio = pronoun.get(0).getCount()/(pronoun.get(0).getCount()+pronoun.get(1).getCount());
		 possessiveRatio = pronoun.get(2).getCount()/(pronoun.get(2).getCount()+pronoun.get(3).getCount());
		 		 
		 double[] feaVector = {0,0,0,0,0,0,0,0};
			 feaVector[0] = Math.round(countHe);
			 feaVector[1] = Math.round(countShe);
			 feaVector[2] = Math.round(countHis);
			 feaVector[3] = Math.round(countHer);
			 feaVector[4] = Math.round(countHeHis);
			 feaVector[5] = Math.round(countSheHer);
			 feaVector[6] = round(pronounRatio,2);
			 feaVector[7] = round(possessiveRatio,2);
		 feature.setFeatureValues(feaVector);
		 return feature;
	}
	 
	 
	 public static Feature countPronounHeurstic(String html,Feature feature,ArrayList<Pronoun> pronoun, String heurstic) {
		 for(int j = 0;j<pronoun.size();j++){
			 int i = 1;
			 Pattern p = Pattern.compile(pronoun.get(j).getPronoun());
			 Matcher m = p.matcher( html.toLowerCase() );
			 while (m.find()) {
			     i++;
			 }
			 pronoun.get(j).setCount(i);
		 }
		 		 
		 float countHe = pronoun.get(0).getCount();
		 float countShe = pronoun.get(1).getCount();
		 
		 float countHis = pronoun.get(2).getCount();
		 float countHer = pronoun.get(3).getCount();
		 
		 float countHeHis = countHe+countHis;
		 float countSheHer = countShe+countHer;
		 
		 
		 double[] feaVector = {0,0};
		 if (heurstic == "he") {
			 feaVector[0] = Math.round(countHe);
			 feaVector[1] = Math.round(countShe);
		}else if (heurstic == "his") {
			feaVector[0] = Math.round(countHis);
			 feaVector[1] = Math.round(countHer);
		}else if (heurstic == "hehis") {
			feaVector[0] = Math.round(countHeHis);
			 feaVector[1] = Math.round(countSheHer);
		}
		 feature.setFeatureValues(feaVector);
		 return feature;
	}
	 
	 
	 
	 
	 
	 public static double round(Double value, int places) {
		    if (places < 0) throw new IllegalArgumentException();
		    long factor = (long) Math.pow(10, places);
		    value = value * factor;
		    long tmp = Math.round(value);
		    return (double) tmp / factor;
		}
	 
	 public static ArrayList<Pronoun> getPronounList() {
		 	ArrayList<Pronoun> Pronouns= new ArrayList<Pronoun>();
		 	Pronoun he = new Pronoun(1, " he ");
			Pronoun she = new Pronoun(1, " she ");
			Pronoun his = new Pronoun(1, " his ");
			Pronoun her = new Pronoun(1, " her ");
			Pronouns.add(he);
			Pronouns.add(she);
			Pronouns.add(his);
			Pronouns.add(her);
			return Pronouns;
	 }
}
