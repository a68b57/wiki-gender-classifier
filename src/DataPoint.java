

public class DataPoint {
	private double[] featureVector;
	private int classLabel;
	private int dims;
	
	public DataPoint(){
		
	}
	
	public DataPoint(double[] featurevalues, int classlbl){
		this.featureVector = featurevalues;
		this.classLabel = classlbl;
		this.dims = featurevalues.length;
	}
	
	public double[] getFeatureValues(){
		return this.featureVector;
	}
	
	public int getClassLabel(){
		return this.classLabel;
	}
	
	public int getFeatureDims(){
		return this.dims;
	}
	
	public double computeDistance(DataPoint B){
		//System.out.print("computing distance between feature f:" + this.toString() + "and point B: " + B.toString() + "\n");
		double distance = 0;
		//simple euclidean dist
		for (int i=6; i<B.dims; ++i){
			distance += Math.pow((this.featureVector[i-6] - B.featureVector[i-6]), 2);
		}
		distance = Math.sqrt(distance);
		//System.out.print("distance is: " + distance + "\n");
		return distance;
	}
	
	public void setClassLabel(int classlbl){
		this.classLabel = classlbl;
	}
	
	public void setFeatureValues(double[] featurevals){
		this.featureVector = featurevals;
		this.dims = featurevals.length;
	}
	
	public String toString(){
		String featVect = "";
		for (double featval : this.featureVector){
			featVect += featval + ", ";
		}
		return "featurevals: " + featVect + ", class: " + this.classLabel;
	}
	
	
}
