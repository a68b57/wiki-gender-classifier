import java.awt.EventQueue;
import java.awt.Font;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JButton;
import javax.swing.JLabel;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class MainActivity extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainActivity frame = new MainActivity();
					frame.setTitle("Gender predication");
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public MainActivity() {
		super("Gender predication");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 591, 424);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JButton btnOnePage = new JButton("I have the URL of a single page");
		btnOnePage.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				SingleURL frame = new SingleURL();
				frame.setVisible(true);
				MainActivity.this.dispose();
			}
		});
		btnOnePage.setBounds(96, 221, 377, 47);
		btnOnePage.setFont(new Font("Arial", Font.PLAIN, 20));
		contentPane.add(btnOnePage);
		
		JLabel lblPutThe = new JLabel("1. Put the training data in the \"train\" folder");
		lblPutThe.setBounds(96, 63, 377, 64);
		lblPutThe.setFont(new Font("Arial", Font.PLAIN, 20));
		contentPane.add(lblPutThe);
		
		JLabel lblSelectOn = new JLabel("2. Select one of the following options:");
		lblSelectOn.setBounds(96, 140, 377, 35);
		lblSelectOn.setFont(new Font("Arial", Font.PLAIN, 20));
		contentPane.add(lblSelectOn);
		
		JButton btnNewButton = new JButton("I have the csv file");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		btnNewButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				ClassifyCsv frame = new ClassifyCsv();
				frame.setVisible(true);
				MainActivity.this.dispose();
			}
		});
		btnNewButton.setBounds(96, 281, 377, 47);
		btnNewButton.setFont(new Font("Arial", Font.PLAIN, 20));
		contentPane.add(btnNewButton);
	}
}
