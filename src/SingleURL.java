import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.*;
//import classifier.Feature;
import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.JTextArea;
import java.awt.Font;
import java.awt.Color;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Vector;
import java.util.stream.Collectors;
import javax.swing.JProgressBar;

public class SingleURL extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JButton btnActivateDataset = new JButton("Initialize training set");
	private JButton btnPredict = new JButton("Classify gender");
	private JButton btnEvaluate = new JButton("Evaluate");
	private ArrayList<Pronoun> Pronouns= new ArrayList<Pronoun>();
	private JProgressBar progressBar = new JProgressBar(0,100);
	private JTextArea textArea = new JTextArea();
	private JTextArea txtrCopyYourUrl = new JTextArea();
	private String csvFile = System.getProperty("user.dir")+"\\train\\traindata_updated.csv";
	private JButton btnHome = new JButton("Home");
	private FileReader fs = null;
	private JRadioButton K1;
	private JRadioButton K3;
	private JRadioButton K5;
	private int K;

	
	public SingleURL() {
			super("Gender predication");
			Pronouns = URLReader.getPronounList();
			initLayout();
			
	}
	
	public void initLayout(){
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 591, 424);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		contentPane.setOpaque(true);
		
		textArea.setBounds(14, 242, 229, 105);
		textArea.setEditable(false);
		textArea.setFont(new Font("Arial", Font.PLAIN, 20));
		textArea.setLineWrap(true);
		textArea.setWrapStyleWord(true);
		contentPane.add(textArea);
	
		
		JLabel lblWikipediaUrl = new JLabel("Copy the WikiPedia URL below:");
		lblWikipediaUrl.setFont(new Font("Arial", Font.PLAIN, 20));
		lblWikipediaUrl.setBounds(14, 13, 303, 46);
		contentPane.add(lblWikipediaUrl);
			
		
		txtrCopyYourUrl.setForeground(Color.BLACK);
		txtrCopyYourUrl.setLineWrap(true);
		txtrCopyYourUrl.setFont(new Font("Arial", Font.PLAIN, 20));
		txtrCopyYourUrl.setBounds(14, 61, 550, 84);
		contentPane.add(txtrCopyYourUrl);
		btnPredict.setToolTipText("Classify the gender of the page of the given URL via KNN");
		
		
		btnPredict.setEnabled(false);
		try {
			fs = new FileReader(csvFile);
		} catch (FileNotFoundException e) {
			
		}
			
			if (fs==null) {
				btnPredict.setEnabled(false);
			}else{
				btnPredict.setEnabled(true);
			}
		
		
		        		
		btnPredict.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				Predict predict = new Predict();
				predict.execute();
				//textArea.setText("Counts from WikiPedia page:\n"+"feature value: "+feature.getFeatureValues()[0]+" "+feature.getFeatureValues()[1]);
				//System.out.println("\nCounts from WikiPedia page:");
				//System.out.println("feature value: "+feature.getFeatureValues()[0]+" "+feature.getFeatureValues()[1]);
			}
		});
		
		
		btnPredict.setFont(new Font("Arial", Font.PLAIN, 20));
		btnPredict.setBounds(257, 242, 208, 46);
		contentPane.add(btnPredict);
		btnEvaluate.setToolTipText("Check for the gender of the entity via Wikidata");
		
		btnEvaluate.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				Evaluate evaluate = new Evaluate();
				evaluate.execute();
			}
		});
		btnEvaluate.setFont(new Font("Arial", Font.PLAIN, 20));
		btnEvaluate.setBounds(257, 301, 208, 46);
		contentPane.add(btnEvaluate);
		
		progressBar.setBounds(241, 170, 299, 46);
		progressBar.setMinimum(0);
		progressBar.setStringPainted(true);
		progressBar.setFont(new Font("Arial", Font.PLAIN, 20));
		contentPane.add(progressBar);
		btnActivateDataset.setToolTipText("Get the feature values for the training set. You can skip this if the \"traindata_updated.csv\" is already in the \"train\" folder");
				       		
		btnActivateDataset.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				UpdateTrainData updateTrainData = new UpdateTrainData();
				updateTrainData.execute();
			}
		});
		btnActivateDataset.setBounds(14, 170, 208, 46);
		btnActivateDataset.setFont(new Font("Arial", Font.PLAIN, 20));	
		contentPane.add(btnActivateDataset);
		btnHome.setToolTipText("Back to the home screen");
		
		btnHome.setBounds(446, 24, 113, 27);
		btnHome.setFont(new Font("Arial", Font.PLAIN, 20));
		contentPane.add(btnHome);
		
		K1 = new JRadioButton("K=1");
		K1.setToolTipText("Look at the nearest neighbour");
		K1.setBounds(475, 242, 83, 32);
		K1.setFont(new Font("Arial", Font.PLAIN, 20));
		contentPane.add(K1);
		
		K3 = new JRadioButton("K=3");
		K3.setToolTipText("Look at three nearest neighbours");
		K3.setBounds(475, 281, 65, 21);
		K3.setSelected(true);
		K3.setFont(new Font("Arial", Font.PLAIN, 20));
		contentPane.add(K3);
		
		K5 = new JRadioButton("K=5");
		K5.setToolTipText("Look at five nearest neighbours");
		K5.setBounds(475, 311, 70, 27);
		K5.setFont(new Font("Arial", Font.PLAIN, 20));
		contentPane.add(K5);
		
		ButtonGroup buttonGroup = new ButtonGroup();
        buttonGroup.add(K1);
        buttonGroup.add(K3);
        buttonGroup.add(K5);
		
		
		
		
		btnHome.addMouseListener(new MouseAdapter() {			
			@Override
			public void mouseClicked(MouseEvent e) {
				MainActivity frame = new MainActivity();
				frame.setVisible(true);
				SingleURL.this.dispose();
			}
		});
			
		
		
	}
	
	
	class UpdateTrainData extends SwingWorker<Void, Void> {
        /*
         * Main task. Executed in background thread.
         */
        @Override
        public Void doInBackground() {
			btnActivateDataset.setEnabled(false);
			int progress = 0;
			setProgress(0);
			Vector<Feature> newTrainSet = CSVReader.updateTrainingSet();
			for(int i=0;i<newTrainSet.size();i++ ){
				String wikiPediaURL = newTrainSet.get(i).getUrl();
				wikiPediaURL = wikiPediaURL.replace(",", "");
				String html = URLReader.getHTML(wikiPediaURL);
				URLReader.countPronounForFeature(html.toLowerCase(), newTrainSet.get(i), Pronouns);
				progress = (i*100)/newTrainSet.size()+1;
				progressBar.setValue(progress);
			}
			
			String recordAsCsv = newTrainSet.stream()
			        .map(Feature::toCsvRow)
			        .collect(Collectors.joining(System.getProperty("line.separator")));
			
			try {
				PrintWriter pw = new PrintWriter(new File(csvFile));
		        StringBuilder sb = new StringBuilder();
		        sb.append(recordAsCsv);
		        pw.write(sb.toString());
		        pw.close();
			} catch (FileNotFoundException e1) {
				e1.printStackTrace();
			}
			btnActivateDataset.setEnabled(true);
			btnPredict.setEnabled(true);
            return null;
        }  
    }
	
	
	class Evaluate extends SwingWorker<Void, Void> {
        /*
         * Main task. Executed in background thread.
         */
        @Override
        public Void doInBackground() {
			String wikiPediaURL = txtrCopyYourUrl.getText();
			if (!wikiPediaURL.equals("")) {
				String wikidataURL = URLReader.getWikidataLink(wikiPediaURL);
				String title = URLReader.getTitle(wikidataURL);
				String gender = URLReader.evaluation(wikidataURL);
				System.out.println("Evaluation from Wikidata:\n"+title+gender);
				textArea.setText("Evaluation from Wikidata:\n"+title+gender);
			}else{
				textArea.setText("Please enter the link of the page first"); 
			}
            return null;
        }  
    }
	
	class Predict extends SwingWorker<Void, Void> {
        /*
         * Main task. Executed in background thread.
         */
        @Override
        public Void doInBackground() {
        	String wikiPediaURL = txtrCopyYourUrl.getText();
        	if (K1.isSelected()) {
				K = 1;
			}else if (K3.isSelected()) {
				K = 3;
			}else if (K5.isSelected()) {
				K = 5;
			}
			if (!wikiPediaURL.equals("")) {
				String html = URLReader.getHTML(wikiPediaURL);
				Feature feature = URLReader.countPronouns(html,Pronouns);
				String gender  = CSVReader.applyKnn(feature,K);
				textArea.setText("Classification by KNN-"+K+":\n"+gender); 
			}else {
				textArea.setText("Please enter the link of the page first"); 
			}
            return null;
        }  
    }
}
