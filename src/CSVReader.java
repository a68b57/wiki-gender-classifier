import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Vector;

//import classifier.Feature;
//import classifier.KNN;

public class CSVReader {
	
	
	public static Vector<Feature> updateTrainingSet(){
		String csvFilename = "\\train\\traindata.csv";
		Vector<Feature>dataset = null;

		try {
			String trainDirectory ="\\train\\";
			String curdir = System.getProperty("user.dir");
			String extractedCSVpathstr = curdir + trainDirectory + "traindata.csv";
			String malepath = curdir + trainDirectory + "\\male\\";
			String femalepath = curdir + trainDirectory + "\\female\\";
			
			try {
				//male
				
				File m = null;
				File[] mpaths;
				m = new File(malepath);
				mpaths = m.listFiles();
				if(mpaths != null){
					for(File mpath:mpaths)
					{
						// prints file and directory paths
						//System.out.println(mpath);
						Tools.ProcessRawCSV(mpath, extractedCSVpathstr, 1);
					}
					}else{
						System.out.println(m.toString() + "is not a directory");
					}
				
				
				
				//female
				
				File f = null;
				File[] fpaths;
				f = new File(femalepath);
				fpaths = f.listFiles();
				if(fpaths != null){
					for(File fpath:fpaths)
					{
						// prints file and directory paths
						//System.out.println(mpath);
						Tools.ProcessRawCSV(fpath, extractedCSVpathstr, 0);
					}
					}else{
						System.out.println(m.toString() + "is not a directory");
					}
				
				
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			dataset = Tools.extractFeaturesFromCSV(csvFilename);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		return dataset;
	}	

		
	
	
	public static String applyKnn(Feature feature,int K) {
		String csvFilename = "\\train\\traindata_updated.csv";
		String gender = null;
		Vector<Feature> dataset = null;
		try {
			//dataset = Tools.extractFeaturesFromCSV(csvFilename);
			dataset = Tools.extractFeaturesFromCSVforCompleteFeature(csvFilename);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}  
		System.out.println(dataset.get(0).getFeatureValues()[6]+"--"+dataset.get(0).getFeatureValues()[7]);
		KNN knnclassifier = new KNN(K, 2); 
		knnclassifier.train(dataset);  
		int genderCLass = knnclassifier.classify(feature);
		if (genderCLass == 1){
			gender = "male";
		}else if(genderCLass == 0){
			gender = "female";
		}else {
			gender = "unknown";
		}
		return gender;
	}
	
	
	public static Vector<Feature> applyKnnforCSV(int K) {
		String csvFilename = "\\train\\traindata_updated.csv";
		String testFileName = "\\test\\test_updated.csv";
		Vector<Feature> dataset = null;
		try {
			dataset = Tools.extractFeaturesFromCSVforCompleteFeature(csvFilename);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} 
		KNN knnclassifier = new KNN(K, 2); 
		
		knnclassifier.train(dataset);  
		Vector<Feature> testset = null;
		try {
			testset = Tools.extractFeaturesFromCSVforCompleteFeature(testFileName);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} 

		
		for(Feature feature : testset){
			int genderClass = knnclassifier.classify(feature);
			feature.setClassLabel(genderClass);
		}
		return testset;
	}
	
	
	public static Vector<Feature> updateTestingSet(){
		String csvFilename = "\\test\\test.csv";
		Vector<Feature>dataset = null;

		try {
			String trainDirectory ="\\test\\";
			String curdir = System.getProperty("user.dir");
			String extractedCSVpathstr = curdir + trainDirectory + "test.csv";
			String malepath = curdir + trainDirectory + "\\male\\";
			String femalepath = curdir + trainDirectory + "\\female\\";
			
			try {
				//male
				
				File m = null;
				File[] mpaths;
				m = new File(malepath);
				mpaths = m.listFiles();
				if(mpaths != null){
					for(File mpath:mpaths)
					{
						// prints file and directory paths
						//System.out.println(mpath);
						Tools.ProcessRawCSV(mpath, extractedCSVpathstr, 1);
					}
					}else{
						System.out.println(m.toString() + "is not a directory");
					}
				
				
				
				//female
				
				File f = null;
				File[] fpaths;
				f = new File(femalepath);
				fpaths = f.listFiles();
				if(fpaths != null){
					for(File fpath:fpaths)
					{
						// prints file and directory paths
						//System.out.println(mpath);
						Tools.ProcessRawCSV(fpath, extractedCSVpathstr, 0);
					}
					}else{
						System.out.println(m.toString() + "is not a directory");
					}
				
				
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			dataset = Tools.extractFeaturesFromCSV(csvFilename);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		return dataset;
	}	

	
	
	
	public static Vector<Feature> applyHeursticForCsv(String heurstic) {
		String testFileName = "\\test\\test_updated.csv";
		Vector<Feature> testset = null;
		try {
			testset = Tools.extractFeaturesFromCSVforCompleteFeature(testFileName);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		
		int genderClass = 0;
		for(Feature feature : testset){
			if (heurstic == "He vs She") {
				if (feature.getFeatureValues()[0]>=feature.getFeatureValues()[1]) {
					genderClass = 1;
				}else{
					genderClass = 0;
				}
			}else if (heurstic == "His vs Her") {
				if (feature.getFeatureValues()[2]>=feature.getFeatureValues()[3]) {
					genderClass = 1;
				}else{
					genderClass = 0;
				}
			}else if (heurstic == "He+His vs She+Her") {
				if (feature.getFeatureValues()[4]>=feature.getFeatureValues()[5]) {
					genderClass = 1;
				}else{
					genderClass = 0;
				}
			}else {
				System.out.println("Unknown error");
			}
			feature.setClassLabel(genderClass);
		}
		return testset;
	}
	
	
}
