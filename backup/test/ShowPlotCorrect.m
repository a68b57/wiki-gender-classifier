function [] = ShowPlotCorrect(filename)
    a = readtable(filename,'Format','%.2f%.2f%.2f%.2f%.2f%.2f%.2f%.2f%.2f%s');
    b = table2array(a(:,7:9));
    malePoint = b(find(b(:,3)==1),:);
    femalePoint = b(find(b(:,3)==0),:);
    h1 = scatter(malePoint(:,1),malePoint(:,2),'x');
    hold on
    h2 = scatter(femalePoint(:,1),femalePoint(:,2),'o');
    xlabel('he/(he+she)');
    ylabel('his/(his+her)');
    title('True classification');
    legend([h1,h2],'male','female','Location','southoutside');
    hold off
end

